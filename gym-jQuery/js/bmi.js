"use strict"

const MINWEIGHT = 1;
const MAXWEIGHT = 777;
const MINHEIGHT = 12;
const MAXHEIGHT = 96;
const MAXUNDER = 18.5;
const MAXOPTIMAL = 25.0;
const MAXOVER = 30.0;

var $ = function(id){
  return document.getElementById(id);
};

var processEntries = function() {
  var height = parseFloat($("height").value);
  var weight = parseFloat($("weight").value);
  var validInput = true;

  if (isNaN(height)){
    $("height").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    setTimeout(function(){
         $("height").nextElementSibling.firstChild.nodeValue = "*";
         },3000);
    validInput = false;
  }
  else if ((height < MINHEIGHT) || (height > MAXHEIGHT)){
    $("height").nextElementSibling.firstChild.nodeValue = "Height input out of range";
    setTimeout(function(){
         $("height").nextElementSibling.firstChild.nodeValue = "*";
         },3000);
    validInput = false;
  }

  if (isNaN(weight)){
    $("weight").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    setTimeout(function(){
         $("weight").nextElementSibling.firstChild.nodeValue = "*";
         },3000);
    validInput = false;
  }
  else if ((weight < MINWEIGHT) || (weight > MAXWEIGHT)){
    $("weight").nextElementSibling.firstChild.nodeValue = "Weight input out of range";
    setTimeout(function(){
         $("weight").nextElementSibling.firstChild.nodeValue = "*";
         },3000);
    validInput = false;
  }

  if (validInput){
    calculateBMI(height, weight);
  }

};

var calculateBMI = function(h, w){
  var bmi = ((w / Math.pow(h, 2)) * 703).toFixed(2);
  $("bmi").value = bmi;
  calculateBMIStatus(bmi);
};


var calculateBMIStatus = function(bmi){
  var s = "";

  if (bmi < MAXUNDER){
    s = "Underweight";
  }
  else if (bmi < MAXOPTIMAL){
    s = "Optimal";
  }
  else if (bmi < MAXOVER){
    s = "Overweight";
  }
  else{
    s = "Obese";
  }

  $("bmiStatus").value = s;

};

var resetTheForm = function() {
  $("height").value = "";
  $("weight").value = "";
  $("bmi").value = "";
  $("bmiStatus").value = "";
  $("height").nextElementSibling.firstChild.nodeValue = "*";
  $("weight").nextElementSibling.firstChild.nodeValue = "*";
  $("height").focus();

}

window.onload=function(){
  $("calculate").onclick = processEntries;
  $("reset").onclick = resetTheForm;
  $("height").focus();
};

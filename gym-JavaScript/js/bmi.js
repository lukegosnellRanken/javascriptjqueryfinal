"use strict"

const MINWEIGHT = 1;
const MAXWEIGHT = 777;
const MINHEIGHT = 12;
const MAXHEIGHT = 96;
const MAXUNDER = 18.5;
const MAXOPTIMAL = 25.0;
const MAXOVER = 30.0;

$(document).ready(function() {

  var processEntries = function() {
    var height = parseFloat($("height").val());
    var weight = parseFloat($("weight").val());
    var validInput = true;

    if (isNaN(height)){
      $("height").nextElementSibling.firstChild.nodeValue = "Numeric input required";
      setTimeout(function(){
           $("height").nextElementSibling.firstChild.nodeValue = "*";
           },3000);
      validInput = false;
    }
    else if ((height < MINHEIGHT) || (height > MAXHEIGHT)){
      $("height").nextElementSibling.firstChild.nodeValue = "Height input out of range";
      setTimeout(function(){
           $("height").nextElementSibling.firstChild.nodeValue = "*";
           },3000);
      validInput = false;
    }

    if (isNaN(weight)){
      $("weight").nextElementSibling.firstChild.nodeValue = "Numeric input required";
      setTimeout(function(){
           $("weight").nextElementSibling.firstChild.nodeValue = "*";
           },3000);
      validInput = false;
    }
    else if ((weight < MINWEIGHT) || (weight > MAXWEIGHT)){
      $("weight").nextElementSibling.firstChild.nodeValue = "Weight input out of range";
      setTimeout(function(){
           $("weight").nextElementSibling.firstChild.nodeValue = "*";
           },3000);
      validInput = false;
    }

    if (validInput){
      calculateBMI(height, weight);
    }

  };

  var calculateBMI = function(h, w){
    var bmi = ((w / Math.pow(h, 2)) * 703).toFixed(2);
    $("bmi").val() = bmi;
    calculateBMIStatus(bmi);
  };


  var calculateBMIStatus = function(bmi){
    var s = "";

    if (bmi < MAXUNDER){
      s = "Underweight";
    }
    else if (bmi < MAXOPTIMAL){
      s = "Optimal";
    }
    else if (bmi < MAXOVER){
      s = "Overweight";
    }
    else{
      s = "Obese";
    }

    $("bmiStatus").val() = s;

  };
});

var resetTheForm = function() {
  $("height").val() = "";
  $("weight").val() = "";
  $("bmi").val() = "";
  $("bmiStatus").val() = "";
  $("height").nextElementSibling.firstChild.nodeValue = "*";
  $("weight").nextElementSibling.firstChild.nodeValue = "*";
  $("height").focus();

}

window.onload=function(){
  $("calculate").onclick = processEntries;
  $("reset").onclick = resetTheForm;
  $("height").focus();
};
